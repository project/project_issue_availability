
DESCRIPTION

This module will allow users who can be assigned issues to specify their
availability for issues based on the classification the of issue (i.e., 
"issue types", via taxonomy), based on a maximum number of active issues, 
or based on given date. The 'Assigned' field provided by the project_issue 
module will be filtered to display only those users who are designated as 
'available' based on the above criteria.

Note that a user who has not selected any issues types is assumed to be 
available for all issue types. A user who has not set a maximum number of 
active issues can be assigned an unlimited number.


INSTALLATION & MINIMALLY REQUIRED CONFIGURATION

1. Install the module as usual
2. Create a Vocabulary containing the Issue Types
3. Visit admin/project/project-issue-settings and select the Vocabulary created above.


BUGS

Please report to http://drupal.org/project/issues/project_issue_availability


AUTHOR

Matt Chapman <Matt@NinjitsuWeb.com>

Ninjitsu Web Development is available for paid implementation and customization 
consultation and services. http://www.NinjitsuWeb.com